#!/usr/bin/python

#Program to identify a person from an image using an existing image dataset

#Import the required modules
import thread
import shutil
import cv2, os
import numpy as np
from PIL import Image

thickness = 2
conf_th = 100
scale = 1.2
min_n = 5
finalconfidences = []
finalneg = []
        
class lbph():
    def init(self):
        #For face detection we use Haar Cascade provided by OpenCV
        print "Using Haar Cascade Classifier"
        cascadePath = "haarcascade_frontalface_default.xml"
        faceCascade = cv2.CascadeClassifier(cascadePath)
        #For face recognition we use LBPH Face Recognizer 
        print "Using LBPH Recognizer"
        recognizer = cv2.createLBPHFaceRecognizer()
        return faceCascade, recognizer

    def get_images_and_labels(self, path):
        #Append all the absolute image paths in a list image_paths_
        image_paths_ = [os.path.join(path, f) for f in os.listdir(path) if not f.endswith('.test')]
        #images contain face images
        images = []
        #labels contain the label that is assigned to the image
        labels = []
        count = 0
        for image_path_ in image_paths_:
            #Read the image and convert it to grayscale
            image_pil = Image.open(image_path_).convert('L')
            #Convert the image format into a numpy array
            image = np.array(image_pil, 'uint8')
            #Get the label of the image
            nbr = int(os.path.split(image_path_)[1].split(".")[0].replace("set", ""))
            #Detect the face in the image
            faces = faceCascade.detectMultiScale(image)
            #If face is detected, append the face to images and the label to labels
            for (x, y, w, h) in faces:
                images.append(image[y: y + h, x: x + w])
                labels.append(nbr)
            count = count + 1
        #print "Number of images for training = " + str(count)
        #Return the images list and labels list
        return images, labels
    
    def main(self, pathp, pathn, patht, small):
	check = 2
	global finalconfidences
	#Append the images with the extension .test into image_paths
        image_paths = [os.path.join(patht, f) for f in os.listdir(patht) if f.endswith('.test')]
       	for image_path in image_paths:
	    #Read the image and convert it into grayscale and then a numpy array
            test_image = cv2.imread(image_path)
	    predict_image_pil = Image.open(image_path).convert('L')
            predict_image = np.array(predict_image_pil, 'uint8')
            #Detect the faces in the image
 	    faces = faceCascade.detectMultiScale(predict_image, float(scale), int(min_n))
    	    for (x, y, w, h) in faces:
                #confidences is a list to store the confidence values of all detected faces in the image. A low confidence value corresponds 			to a greater match of detected face with the faces in the training set. The value 0.0 represents an ideal match
                #Obtain labels from test image and training set and check for matching. If equal, the confidence value is appended to the list 			confidence
		nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
        	nbr_actual = int(os.path.split(image_path)[1].split(".")[0].replace("set", ""))
	    #print finalconfidences
            #Find minimum confidence value from the list
	    if len(finalconfidences)!=0:
	        print "Processing image " +image_path
	        print "Minimum confidence value = " + str(small)
	        for (x, y, w, h) in faces:
                    nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
               	    nbr_actual = int(os.path.split(image_path)[1].split(".")[0].replace("set", ""))
            	    #If confidence value of any face equals the minimum confidence value, then Face Recognition is correct
                    if conf == small and conf<=int(conf_th):
    	                crop_img = test_image[y: y + h, x: x + w]
    	                cv2.rectangle(test_image,(x,y),(x+w,y+h),(255,255,255),thickness)
    	                cv2.namedWindow('Image Window', cv2.WINDOW_NORMAL)
			cv2.imshow('Image Window',test_image)
 			print "Is Face Recognition Correct ? Add this image to dataset ?"
			print "Press 'y' to add to Positive Dataset and 'n' to add to Negative Dataset"
			k = cv2.waitKey()
			if k == ord('y'):
			    check=1
			    cv2.destroyAllWindows()
			elif k == ord('n'):
			    check=0
			    cv2.destroyAllWindows()
			else:
			    check=2
			    cv2.destroyAllWindows()
	            if conf == small and conf<=int(conf_th) and check == 1:   
    	                path, dirs, files = os.walk(pathp).next()
	                file_count = len(files)
	                #Add the cropped image of face to dataset if it is found to be matching
		        flag = 1
    	                while (flag == 1):
    	                    src="set0" + str(nbr_predicted) + ".rec" + str(file_count)
    	                    if not os.path.exists(path + "/" + src):
    	    	                cv2.imwrite("set0" + str(nbr_predicted) + ".rec" + str(file_count) +".jpg", crop_img)
    	                        os.rename("set0" + str(nbr_predicted) + ".rec" + str(file_count) +".jpg","set0" + str(nbr_predicted) + ".rec" + str (file_count))
    	                        shutil.move(src, path)
			        print "Adding image to Positive Dataset..."
			        flag = 0
    	                        break
    	                    else:
    	                        file_count = file_count + 1
    	                break  
    	            if conf == small and conf<=int(conf_th) and check == 0:
    	                path, dirs, files = os.walk(pathn).next()
	                file_count = len(files)
		        flag = 1
    	                while (flag == 1):
    	                    src="set0" + str(nbr_predicted) + ".rec" + str(file_count)
    	                    if not os.path.exists(path + "/" + src):
    	    	                cv2.imwrite("set0" + str(nbr_predicted) + ".rec" + str(file_count) +".jpg", crop_img)
    	                        os.rename("set0" + str(nbr_predicted) + ".rec" + str(file_count) +".jpg","set0" + str(nbr_predicted) + ".rec" + str (file_count))
    	                        shutil.move(src, path)
			        print "Adding image to Negative Dataset..."
    	                        flag = 0
    	                        break
    	                    else:
    	                        file_count = file_count + 1
			break
    	                
            else:
                print "No faces were detected..." 
        return finalconfidences
    
    def conf_check(self, patht, pathn):
	finalconfidences = []
	global finalneg
        #Append the images with the extension .test into image_paths
        image_paths = [os.path.join(patht, f) for f in os.listdir(patht) if f.endswith('.test')]
       	for image_path in image_paths:
	    #Read the image and convert it into grayscale and then a numpy array
            test_image = cv2.imread(image_path)
	    predict_image_pil = Image.open(image_path).convert('L')
            predict_image = np.array(predict_image_pil, 'uint8')
            #Detect the faces in the image
 	    faces = faceCascade.detectMultiScale(predict_image, float(scale), int(min_n))
    	    for (x, y, w, h) in faces:
                #confidences is a list to store the confidence values of all detected faces in the image. A low confidence value corresponds 			to a greater match of detected face with the faces in the training set. The value 0.0 represents an ideal match
                #Obtain labels from test image and training set and check for matching. If equal, the confidence value is appended to the list 			confidence
		nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
        	nbr_actual = int(os.path.split(image_path)[1].split(".")[0].replace("set", ""))
                if nbr_actual == nbr_predicted:
            	    finalconfidences.append(conf)    
		    negimages, neglabels = l.get_images_and_labels(pathn)
		    recognizer.train(negimages, np.array(neglabels)) 
		    nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
		    finalneg.append(conf)
		posimages, poslabels = l.get_images_and_labels(pathp)
		recognizer.train(posimages, np.array(poslabels)) 
        return finalconfidences, finalneg

l = lbph()    
#Input path to dataset
path = raw_input("Enter path to dataset: ")
patht = raw_input("Enter path to test image: ")

#path = "/home/devika/nivin"
#patht= "/home/devika/nivintest"
pathp = path + "/Pos"
pathn = path + "/Neg"

#Call get_images_and_labels function to get face images from the dataset and corresponding labels
ch0 = raw_input("Threshold Confidence Level is set to 100. Do you wish to change it? (y/n) ")
if ch0 == 'y':
    conf_th = raw_input("Enter new Threshold Confidence: ")
ch1 = raw_input("Scale Factor is set to 1.2. Do you wish to change it? (y/n) ")
if ch1 == 'y':
    scale = raw_input("Enter new Scale Factor: ")
ch2 = raw_input("Minimum Neighbors is set to 5. Do you wish to change it? (y/n) ")
if ch2 == 'y':
    min_n = raw_input("Enter new Minimum Neighbors: ")



faceCascade, recognizer = l.init()
print "Training the positive recognizer..."
posimages, poslabels = l.get_images_and_labels(pathp)
recognizer.train(posimages, np.array(poslabels)) 
finalconfidences, finalneg = l.conf_check(patht, pathn)

for i in range(len(finalneg)):
    if finalneg[i]>=0 and finalneg[i]<=50:
        print "Negative Confidence "+ str(finalneg[i]) +" is Good!"
    elif finalneg[i]>50 and finalneg[i]<=200:
	print "Negative Confidence "+ str(finalneg[i]) +" is Average!"
    elif finalneg[i]>200:
	print "Negative Confidence "+ str(finalneg[i]) +" is Poor!"

if len(finalconfidences)!=0:
    posconf = min(finalconfidences)
    ind = finalconfidences.index(min(finalconfidences))

print "Positive Confidences for detected faces = "+str(finalconfidences)
print "Minimum positive confidence = "+str(posconf)
negconf =  finalneg[ind]

print "Negative Confidences for detected faces = "+str(finalneg)
print "Required Negative confidence = "+str(negconf)

if negconf > posconf and negconf>50:
   # faceCascade, recognizer = l.init()
   # posimages, poslabels = l.get_images_and_labels(pathp)
   # recognizer.train(posimages, np.array(poslabels)) 
    l.main(pathp, pathn, patht, posconf)

else:
    print "Identified Wrong Recognition..."
    del finalconfidences[ind]
    del finalneg[ind]
    while finalconfidences:
    	    
	print "Training the positive recognizer again..."
    	posimages, poslabels = l.get_images_and_labels(pathp)
    	recognizer.train(posimages, np.array(poslabels)) 
	if len(finalconfidences)!=0:
    	    posconf = min(finalconfidences)
  	    ind = finalconfidences.index(min(finalconfidences))
        print "Minimum positive confidence = "+str(posconf)
        faceCascade, recognizer = l.init()
        print "Training the negative recognizer again..."
	negimages, neglabels = l.get_images_and_labels(pathn)
        recognizer.train(negimages, np.array(neglabels)) 
        negconf =  finalneg[ind]
        print "Required Negative confidence = "+str(negconf)
        
	if negconf > posconf and negconf>50 :
    	    faceCascade, recognizer = l.init()
            posimages, poslabels = l.get_images_and_labels(pathp)
    	    recognizer.train(posimages, np.array(poslabels)) 
    	    finalconfidences = l.main(pathp, pathn, patht, posconf)
            finalconfidences[:] = []
	else:
            print "Identified Wrong Recognition..."
            del finalconfidences[ind]
    
    

