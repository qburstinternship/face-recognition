#!/usr/bin/python

import shutil
import cv2, os
import numpy as np
from PIL import Image
from lbph import lbph
from rating import rating
from Tkinter import *
import tkMessageBox
import Tkinter

import sys

path = sys.argv[1]
patht = sys.argv[2]
#path='/home/dil/obama'
#patht='/home/dil/test'
l = lbph()     
pathp = path + "/Pos"
pathn = path + "/Neg"
conf_th = 100  
scale = 1.2
min_n = 5

faceCascade, recognizerp = l.detect()
print "Training the positive recognizer..."
print  pathp
posimages, poslabels = l.get_images_and_labels(pathp, faceCascade, recognizerp)
recognizerp.train(posimages, np.array(poslabels))

faceCascade, recognizern = l.detect()
print "Training the negative recognizer..."
negimages, neglabels = l.get_images_and_labels(pathn, faceCascade, recognizern)
recognizern.train(negimages, np.array(neglabels)) 

image_paths = [os.path.join(patht, f) for f in os.listdir(patht) if f.endswith('.jpg')]
for image_path in image_paths:
    l.conf_select(patht, pathp, pathn, image_path, faceCascade, recognizerp, recognizern )

r = rating()

try:
    target = open("test1.txt")
    content = target.read()
except IOError:
    target = open("test1.txt", 'w+')
    num = 0
    target.write(str(num))
    target.write("\n")
    content = "0\n"
intc = int(content.strip())
if intc<5:    
    intc = intc + 1
if intc==5:
    r.display()
    intc=0
target = open("test1.txt",'w')
target.truncate()   
target.write(str(intc))


