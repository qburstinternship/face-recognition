import sys
import cv2, os
import numpy as np
from PIL import Image
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
recognizer = cv2.createEigenFaceRecognizer()
thickness = 2
def fun(path):
    image_paths = [os.path.join(path, f) for f in os.listdir(path) if not f.endswith('.test')]
    images = []
    labels = []
    for image_path in image_paths:
        image_pil = Image.open(image_path).convert('L')
        image = np.array(image_pil, 'uint8')
        nbr = int(os.path.split(image_path)[1].split(".")[0].replace("set", ""))
        print nbr
        faces = faceCascade.detectMultiScale(image)
        for (x, y, w, h) in faces:
            print "yes"+image_path
            
            images.append(image[y: y + h, x: x + w])
            labels.append(nbr)
            cv2.imshow("Adding faces to traning set...", image[y: y + h, x: x + w])
            cv2.waitKey()
    return images,labels

path = '/home/dil/at1/64'
images,labels = fun(path)
recognizer.train(images, np.array(labels))

print "training completed"

image_paths = [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.test')]

for image_path in image_paths:
    predict_image_pil = Image.open(image_path).convert('L')
    print predict_image_pil
    predict_image = np.array(predict_image_pil, 'uint8')
    faces = faceCascade.detectMultiScale(predict_image)
    for (x, y, w, h) in faces:
        print "yes test"
        nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
        print image_path
        cv2.rectangle(predict_image,(x,y),(x+w,y+h),(255,0,0),thickness)
       # crop_img = image_path[y: y + h, x: x + w]
    	print "Displaying image with recognized face..."
    	cv2.imshow("Adding faces to traning set...", predict_image[y: y + h, x: x + w])
        cv2.waitKey()
