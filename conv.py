#!/usr/bin/python

#Program to identify a person from an image using an existing image dataset

#Import the required modules
import shutil
import sys
import cv2, os
import numpy as np
from PIL import Image
from PIL import ImageDraw
from django.conf import settings
thickness = 2
height = 230
width = 230       

class lbph():
    def init(self):
        #For face detection we use Haar Cascade provided by OpenCV
        print "Using Haar Cascade Classifier"
        cascadePath = "haarcascade_frontalface_default.xml"
        faceCascade = cv2.CascadeClassifier(cascadePath)
        #For face recognition we use LBPH Face Recognizer 
        print "Using LBPH Recognizer"
        recognizer = cv2.createEigenFaceRecognizer()
        return faceCascade, recognizer

    def get_images_and_labels(self, path,sz=None):
        fl=1
        mov="/home/dil/at1/temp/"
                    
        #Append all the absolute image paths in a list image_paths_
        image_ts = [os.path.join(patht, f) for f in os.listdir(patht) if f.endswith('.test')]
        while(fl):
            for img_t in image_ts:
                image_t=cv2.imread(img_t)
            
                image_t = Image.open(img_t)
            
                image_t=image_t.resize(sz, Image.ANTIALIAS)
                image_t.save(mov+"set01" +"_.jpg")              
                      
            fl=0        
        
        image_paths_ = [os.path.join(path, f) for f in os.listdir(path) if not f.endswith('_.jpg')]
        #images contain face images
        images = []
        #labels contain the label that is assigned to the image
        labels = []
        count = 0
        
        for image_path_ in image_paths_:
            #Read the image and convert it to grayscale
            image_move=cv2.imread(image_path_)
            image_move = Image.open(image_path_)
            
            image_pil = Image.open(image_path_).convert('L')
            #Convert the image format into a numpy array
            imagef = np.array(image_pil, 'uint8')
            
            #Get the label of the image
            nbr = int(os.path.split(image_path_)[1].split(".")[0].replace("set", ""))
        ##    nbr=count        
            #Detect the face in the image
            faces = faceCascade.detectMultiScale(imagef)
            #If face is detected, append the face to images and the label to labels
            
            try:
                print "changing size from " +str(image_move.size)
                if(sz is not None):
                    
                    mov, dirs, files = os.walk(mov).next()
         
                    file_count = len(files)
	            image_move=image_move.resize(sz, Image.ANTIALIAS)
                  
                    #image_pil = cv2.resize(imagef, sz)
                    #image = cv2.imread(os.path.join(image_paths_, image), cv2.IMREAD_GRAYSCALE)
         ##           image_pil = image_pil.resize(sz, Image.ANTIALIAS)                   
                    print "to " +str(image_move.size)
                    #image_t = image_t.resize(sz, Image.ANTIALIAS)                    
                    
                    image_move.save(mov+"set0"  + str(file_count) +".jpg")
                    print image_move
                   
         ##           image_move=cv2.imread(image_move)
         ##           cv2.imshow("Recogniz", image_move)
         ##           cv2.waitKey()
         ##           cv2.destroyAllWindows()
                    #mov, dirs, files = os.walk(mov).next()
	            #imwrite(,image_)
                    #shutil.move(image_move,mov)
                    #cv2.imwrite(mov+"set0" + str(nbr) + ".rec" , image_mov)   
                   
                    

                    
                    
                    
                    
        ##            image = np.array(image_pil, 'uint8')
            except IOError, (errno, strerror):
                print "I/O error({0}): {1}".format(errno, strerror)
            except:
                print "Unexpected error:", sys.exc_info()[0]
                raise
        image_move_ = [os.path.join(mov, f) for f in os.listdir(mov) if not f.endswith('_.jpg')]
         
        for img in image_move_:
            img_= Image.open(img).convert('L')
            image = np.array(img_, 'uint8')
            faces = faceCascade.detectMultiScale(image)
             
            
            
            for (x, y, w, h) in faces:
                print "yes"                
                images.append(image[y: y + h, x: x + w])
                labels.append(nbr)
                
                #cv2.resize(image, image,(78,78), CV_INTER_LINEAR)
                #if (sz is not None):
                 #       im = im.resize(self.sz, Image.ANTIALIAS)
                    #X.append(np.asarray(im, dtype=np.uint8))
                    #y.append(c)
               # Give the image a standard brightness and contrast.
                
          #      if (image):
         #	    cvReleaseImage(&image)


            
            count = count + 1
        print "Number of images for training = " + str(count)
        #Return the images list and labels list
        return images, labels
    
    def main(self, path, patht,mov):
        #Append the images with the extension .test into image_paths
        image_paths = [os.path.join(mov, f) for f in os.listdir(mov) if f.endswith('_.jpg')]
        for image_path in image_paths:
            #Read the image and convert it into grayscale and then a numpy array
            test_image = cv2.imread(image_path)
            predict_image_pil = Image.open(image_path).convert('L')
            #image_pil = image_pil.resize(Standard_size)
            
            predict_image = np.array(predict_image_pil, 'uint8')
            #Detect the faces in the image
            faces = faceCascade.detectMultiScale(predict_image)
            #confidences is a list to store the confidence values of all detected faces in the image. A low confidence value corresponds to a greater match of detected face with the faces in the training set. The value 0.0 represents an ideal match
            confidences = []
    	    for (x, y, w, h) in faces:
    	        #Obtain labels from test image and training set and check for matching. If equal, the confidence value is appended to the list confidence
                nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
        	nbr_actual = int(os.path.split(image_path)[1].split(".")[0].replace("set", ""))
        	if nbr_actual == nbr_predicted:
            	    confidences.append(conf)
        #Find minimum confidence value from the list
	small = min(confidences)
	print "Confidence values of detected faces in test image = " + str(confidences)
	print "Minimum confidence value = " + str(small)
	for (x, y, w, h) in faces:
            nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
            nbr_actual = int(os.path.split(image_path)[1].split(".")[0].replace("set", ""))
            if nbr_actual == nbr_predicted:
                confidences.append(conf)
      	    #If confidence value of any face equals the minimum confidence value, then Face Recognition is correct
      	    if conf == small:
    	        cv2.rectangle(test_image,(x,y),(x+w,y+h),(255,0,0),thickness)
                crop_img = test_image[y: y + h, x: x + w]
    	        print "Displaying image with recognized face..."
    	        cv2.imshow("Recognized Face", test_image)
    	        cv2.waitKey()
 	    cv2.destroyAllWindows()

l = lbph()    
#Input path to dataset and the test image
path = raw_input("Enter path to dataset: ")
patht = raw_input("Enter path to test image: ")
mov="/home/dil/at1/temp"
faceCascade, recognizer = l.init()
#Call get_images_and_labels function to get face images from the dataset and corresponding labels
print "Training the recognizer..."
#settings.configure()
#path = os.path.join(settings.MEDIA_ROOT, 'normalized')
#file_count_ =1
images, labels = l.get_images_and_labels(path,(128,150))
cv2.destroyAllWindows()
#Perform the tranining
for crop in images:
    cv2.imshow("Recognized Face", crop)
    cv2.waitKey()
    print crop
   
print "labels"+str(labels)  
    
recognizer.train(images, np.array(labels))
print "training completed"
l.main(path, patht,mov)

