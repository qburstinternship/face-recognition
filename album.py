#!/usr/bin/python

#Program to identify a person from an album of images using an existing image dataset

#Import the required modules
import shutil
import cv2, os
import numpy as np
from PIL import Image

conf_th = 100
thickness = 2
scale = 1.2
min_n = 5
        
class lbph():
    def init(self):
	#For face detection we use Haar Cascade provided by OpenCV
	print "Using Haar Cascade Classifier"
        cascadePath = "haarcascade_frontalface_default.xml"
        faceCascade = cv2.CascadeClassifier(cascadePath)
	#For face recognition we use LBPH Face Recognizer 
        print "Using LBPH Recognizer"
        recognizer = cv2.createLBPHFaceRecognizer()
        return faceCascade, recognizer

    def get_images_and_labels(self, path):
	#Append all the absolute image paths in a list image_paths_
        image_paths_ = [os.path.join(path, f) for f in os.listdir(path) if not f.endswith('.test')]
        #images contain face images
	images = []
        #labels contain the label that is assigned to the image
	labels = []
	count = 0
        for image_path_ in image_paths_:
	    #Read the image and convert it to grayscale
            image_pil = Image.open(image_path_).convert('L')
            #Convert the image format into a numpy array
            image = np.array(image_pil, 'uint8')
            #Get the label of the image
	    nbr = int(os.path.split(image_path_)[1].split(".")[0].replace("set", ""))
            #Detect the face in the image
            faces = faceCascade.detectMultiScale(image, float(scale), int(min_n))
            #If face is detected, append the face to images and the label to labels
            for (x, y, w, h) in faces:
                images.append(image[y: y + h, x: x + w])
                labels.append(nbr)
            count = count + 1
        print "Number of images for training = " + str(count)
        #Return the images list and labels list
        return images, labels, nbr
    
    def main(self, pathp, pathn, patht, nbr):
	add_cnt = 0
	neg_cnt = 0
	confidences = []
	check = 2
	#Append the images with the extension .test into image_paths
        image_paths = [os.path.join(patht, f) for f in os.listdir(patht) if f.endswith('.jpg')]
        print "The images are: "
	print image_paths
       	for image_path in image_paths:
	    #Read the image and convert it into grayscale and then a numpy array
            test_image = cv2.imread(image_path)
	    predict_image_pil = Image.open(image_path).convert('L')
            predict_image = np.array(predict_image_pil, 'uint8')
            #Detect the faces in the image
 	    faces = faceCascade.detectMultiScale(predict_image, float(scale), int(min_n))
    	    for (x, y, w, h) in faces:
                #confidences is a list to store the confidence values of all detected faces in the image. A low confidence value corresponds 			to a greater match of detected face with the faces in the training set. The value 0.0 represents an ideal match
		confidences = []
                #Obtain labels from test image and training set and check for matching. If equal, the confidence value is appended to the list 			confidence
		nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
        	nbr_actual = nbr
                if nbr_actual == nbr_predicted:
            	    confidences.append(conf)
            #Find minimum confidence value from the list
	    if len(confidences)!=0:
	        small = min(confidences)
	        print "Processing image " +image_path
	        print "Minimum confidence value = " + str(small)
	        for (x, y, w, h) in faces:
                    nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
                    nbr_actual = nbr
            	    #If confidence value of any face equals the minimum confidence value, then Face Recognition is correct
                    if conf == small and conf<=int(conf_th):
    	                crop_img = test_image[y: y + h, x: x + w]
    	                cv2.rectangle(test_image,(x,y),(x+w,y+h),(255,255,255),2)
    	                cv2.namedWindow('Image Window', cv2.WINDOW_NORMAL)
		        cv2.imshow('Image Window',test_image)
			k = cv2.waitKey()
			if k == ord('y'):
			    check=1
			    cv2.destroyAllWindows()
			elif k == ord('n'):
			    check=0
			    cv2.destroyAllWindows()
			else:
			    check=2
			    cv2.destroyAllWindows()
		    if conf == small and conf<=int(conf_th) and check == 1:   
    	                path, dirs, files = os.walk(pathp).next()
	                file_count = len(files)
	                #Add the cropped image of face to dataset if it is found to be matching
		        flag = 1
    	                while (flag == 1):
    	                    src="set0" + str(nbr_predicted) + ".rec" + str(file_count)
    	                    if not os.path.exists(path + "/" + src):
    	    	                cv2.imwrite("set0" + str(nbr_predicted) + ".rec" + str(file_count) +".jpg", crop_img)
    	                        os.rename("set0" + str(nbr_predicted) + ".rec" + str(file_count) +".jpg","set0" + str(nbr_predicted) + ".rec" + str (file_count))
    	                        shutil.move(src, path)
			        print "Adding image to Positive Dataset..."
			        add_cnt = add_cnt + 1
	                        flag = 0
    	                        break
    	                    else:
    	                        file_count = file_count + 1
    	                break  
    	            if conf == small and conf<=int(conf_th) and check == 0:
    	                path, dirs, files = os.walk(pathn).next()
	                file_count = len(files)
		        flag = 1
    	                while (flag == 1):
    	                    src="set0" + str(nbr_predicted) + ".rec" + str(file_count)
    	                    if not os.path.exists(path + "/" + src):
    	    	                cv2.imwrite("set0" + str(nbr_predicted) + ".rec" + str(file_count) +".jpg", crop_img)
    	                        os.rename("set0" + str(nbr_predicted) + ".rec" + str(file_count) +".jpg","set0" + str(nbr_predicted) + ".rec" + str (file_count))
    	                        shutil.move(src, path)
			        print "Adding image to Negative Dataset..."
			        neg_cnt = neg_cnt + 1
    	                        flag = 0
    	                        break
    	                    else:
    	                        file_count = file_count + 1

        print "Added " + str(add_cnt) + " image(s) to dataset with confidence < " + str(conf_th)    
        print "Added " + str(neg_cnt) + " image(s) to negative dataset with confidence < " + str(conf_th)
                    
            
l = lbph()    
#Input path to dataset
path = raw_input("Enter path to dataset: ")
#path = "/home/devika/obama"
pathp = path + "/Pos"
pathn = path + "/Neg"
#patht = "/home/devika/obamatest"
patht = raw_input("Enter path to test files: ")
faceCascade, recognizer = l.init()
#Call get_images_and_labels function to get face images from the dataset and corresponding labels
print "Training the recognizer..."
images, labels, nbr = l.get_images_and_labels(pathp)
#Perform the tranining
recognizer.train(images, np.array(labels))

ch0 = raw_input("Threshold Confidence Level is set to 100. Do you wish to change it? (y/n) ")
if ch0 == 'y':
    conf_th = raw_input("Enter new Threshold Confidence: ")
ch1 = raw_input("Scale Factor is set to 1.2. Do you wish to change it? (y/n) ")
if ch1 == 'y':
    scale = raw_input("Enter new Scale Factor: ")
ch2 = raw_input("Minimum Neighbors is set to 5. Do you wish to change it? (y/n) ")
if ch2 == 'y':
    min_n = raw_input("Enter new Minimum Neighbors: ")
l.main(pathp, pathn, patht, nbr)

